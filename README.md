# KeepMD: Convert Google Keep notes to Markdown

Converts Google Keep notes to Markdown for use with Obsidian.

Requires Google Takeout archive (.tar.gz) containing Keep files (https://takeout.google.com/).


The following conversions are applied:
- The title becomes the name of the file and an H1 heading at the top.
  - See [Untitled Notes](#untitled-notes) section for rules on naming notes without titles.
- Labels become tags.
- Note colours become tags with the colour name (lowercase, e.g., #yellow).
  - The colour value "DEFAULT" is ignored.
- Archived notes are placed in a subdirectory called "Archive".
- Trashed notes are ignored.
- Share information is ignored.
- Pinned information is ignored.
- The 'userEditedTimestampUsec' is used to set the mtime of the file on disk.


## Untitled notes

Notes with an empty `title` field are named based on the following rules:
1. If the note contains annotations, the title of the first annotations is used
2. If the note does not contain annotations, the first (non-empty) line of the text content is used.
3. If the note does not contain text content, the date (in ISO 8601) is used.

If a note with the same name exists, based on any of the above rules, a number in parentheses is appended to subsequent files (i.e., excluding the first one).
