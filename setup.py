from setuptools import setup, find_packages

setup(
    name="keepmd",
    version="1.0",
    description="Convert Google Keep notes to Markdown for use with Obsidian",
    author="Achilleas Koutsou",
    author_email="achilleas.k@gmail.com",
    url="https://gitlab.com/achkts/KeepMD",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "keepmd = keepmd.main:main",
        ],
    },
)
