"""
Converts Google Keep notes to Markdown for use with Obsidian.

Creates all files in a subdirectory called "Keep" in the working directory.
"""
import os
import sys
import re
import json
import pathlib as pl
import argparse
import tarfile
from typing import List, Dict, Any, Optional
from datetime import datetime


TARGET_DIR = "Keep"


def die(msg: str):
    print(msg, file=sys.stderr)
    sys.exit(1)


def fix_attachment_name(fname: str) -> str:
    if fname.endswith(".jpeg"):
        # Attachments are jpeg, but files are jpg
        fname = fname[:-4] + "jpg"
    return fname


def usec_to_datestr(usec: int) -> str:
    return datetime.fromordinal(usec//1000000).isoformat()


def collect_keep_json(tfp: tarfile.TarFile) -> List[str]:
    """
    Returns a list of paths to Google Keep JSON files found inside the tarfile.
    """
    keeppaths: List[str] = list()
    for name in tfp.getnames():
        if re.match(r"Takeout/Keep/.*\.json", name):
            keeppaths.append(name)

    return keeppaths


def read_tar_json(tfp: tarfile.TarFile, path: str) -> Optional[Dict[str, Any]]:
    member = tfp.getmember(path)
    if not member.isfile():
        print("Not a file: {path}. Ignoring.")
        return None
    jsonfp = tfp.extractfile(member)
    if not jsonfp:
        return None

    return json.loads(jsonfp.read())


def make_title(notedata: Dict[str, Any]) -> str:
    if title := notedata.get("title"):
        return title

    if annotations := notedata.get("annotations"):
        if title := annotations[0].get("title"):
            return title

    if text := notedata.get("textContent"):
        if lines := text.strip().split("\n"):
            if title := lines[0]:
                return title
    if timestamp := notedata.get("userEditedTimestampUsec"):
        return usec_to_datestr(int(timestamp))

    # if all else fails, just return "untitled"
    return "untitled"


def format_annotation(annotation: Dict[str, str]) -> str:
    title = annotation["title"]
    url = annotation["url"]
    description = annotation["description"]
    return f"[{title}]({url})\n{description}"


def format_attachment(attachment: Dict[str, str]) -> str:
    """
    Simply returns the embed format on the filepath.
    Does no checking to see if the type of embedded file is supported.
    """

    filepath = fix_attachment_name(attachment["filePath"])
    filepath = os.path.join("attachments", filepath)
    return f"![[{filepath}]]"


def save_attachment(tfp: tarfile.TarFile, attachment: Dict[str, str])\
        -> Optional[str]:
    fname = fix_attachment_name(attachment["filePath"])
    internal_path = f"Takeout/Keep/{fname}"
    attachments_dir = os.path.join(TARGET_DIR, "attachments")
    os.makedirs(attachments_dir, exist_ok=True)
    target_path = os.path.join(attachments_dir, fname)
    attachment_fp = tfp.extractfile(internal_path)
    if not attachment_fp:
        print("Warning: Attachment {fname} not found", file=sys.stderr)
        return None
    content = attachment_fp.read()
    with open(target_path, "wb") as trgt:
        trgt.write(content)
    return target_path


def make_filepath(title: str) -> pl.Path:
    fname = os.path.join(TARGET_DIR, title.replace("/", "_"))
    if os.path.exists(fname):
        fnum = 1
        while os.path.exists(f"{fname} ({fnum})"):
            fnum += 1
        fname = f"{fname} ({fnum})"

    return pl.Path(fname + ".md")


def create_md(notedata: Dict[str, Any]) -> Optional[str]:
    """
    Creates a markdown file from the contents of the dictionary and returns
    the path to the file.
    """
    if notedata["isTrashed"]:
        return None

    title = make_title(notedata)
    fpath = make_filepath(title)

    os.makedirs(TARGET_DIR, exist_ok=True)

    content = ""

    content += f"# {title}\n\n"
    if text := notedata.get("textContent"):
        content += f"{text}\n"

    if checklist := notedata.get("listContent"):
        content += "\n"
        for item in checklist:
            if item["isChecked"]:
                checkbox = "[x]"
            else:
                checkbox = "[ ]"
            itemtext = item["text"]
            content += f"- {checkbox} {itemtext}\n"
        content += "\n"

    for annotation in notedata.get("annotations", list()):
        content += format_annotation(annotation) + "\n"

    for attachment in notedata.get("attachments", list()):
        content += format_attachment(attachment) + "\n"

    for label in notedata.get("labels", list()):
        labelname = label["name"]
        content += f"#{labelname}\n"

    colour = notedata.get("color", "DEFAULT")
    if colour != "DEFAULT":
        content += f"#{colour}\n"

    fpath.write_text(content)

    return str(fpath)


def touch(filepath: str, timestamp: int):
    ts_sec = timestamp * 1000  # timestamp is in micro-seconds
    os.utime(filepath, ns=(ts_sec, ts_sec))  # setting using nanoseconds


def save_files(tfp: tarfile.TarFile, paths: List[str]):
    for path in paths:
        if j := read_tar_json(tfp, path):
            if newfile := create_md(j):
                touch(newfile, j["userEditedTimestampUsec"])
                print(f"Created {newfile}")
            for attachment in j.get("attachments", list()):
                att_fname = save_attachment(tfp, attachment)
                print(f"Extracted attachment {att_fname}")


def list_archive(tfpath: pl.Path):
    if not tfpath.exists():
        die(f"No such file: {tfpath}")
    if not tarfile.is_tarfile(str(tfpath)):
        die(f"Not a tar file: {tfpath}")

    with tarfile.open(tfpath) as tfp:
        keeppaths = collect_keep_json(tfp)
        save_files(tfp, keeppaths)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    archive_description = ("Google Takeout archive (.tar.gz) containing "
                           "Keep files (https://takeout.google.com/)")
    parser.add_argument("archive", help=archive_description)
    args = parser.parse_args()

    tfpath = pl.Path(args.archive)
    list_archive(tfpath)


if __name__ == "__main__":
    main()
